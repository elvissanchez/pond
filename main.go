package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/endpoints"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
)

type Location struct {
	Point   Point  `json:"point"`
}

type Point struct {
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
}

type Pound struct {
	Id                string   `json:"id"`
	Temperature       float32  `json:"temperature"`
	WaterLevel        float32  `json:"water_level"`
	WaterTransparency float32  `json:"water_transparency"`
	WaterDepth        float32  `json:"water_depth"`
	Ph                int32    `json:"ph"`
	Location          Location `json:"location"`
}

func main() {
}
